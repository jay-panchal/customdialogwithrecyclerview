package com.example.customdialogwithrecyclerview;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        load = findViewById(R.id.load);
        load.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Fragment1 fragment1 = new Fragment1();
                fragment1.show(getSupportFragmentManager(), "custom dialog with recyclerview");
            }
        });
    }

}
package com.example.customdialogwithrecyclerview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Fragment1 extends DialogFragment {

    RecyclerView recyclerView;
    Button btnOk, btnCancel;
    TextView tvtitle;

    private Adapter adapter;
    private ArrayList<Pojo> arrayList;

    public Fragment1() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_1, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.RecyclerView1);

        btnOk = view.findViewById(R.id.btnOk);
        btnCancel = view.findViewById(R.id.btnCancel);

        tvtitle = view.findViewById(R.id.tvTitle);
        arrayList = new ArrayList<>();

        createData();

        adapter = new Adapter(arrayList);

        ItemTouchHelper.SimpleCallback itemTouchHelper=new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {


                arrayList.remove(viewHolder.getAdapterPosition());
                adapter.notifyDataSetChanged();


            }
        };

        new ItemTouchHelper(itemTouchHelper).attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(adapter);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "you clicked ok", Toast.LENGTH_SHORT).show();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                Toast.makeText(getActivity(), "you chose cance", Toast.LENGTH_SHORT).show();

            }
        });


    }

    public void createData() {

        Pojo pojo = new Pojo();
        pojo.setItem("jay");
        arrayList.add(pojo);

        Pojo pojo1 = new Pojo();
        pojo1.setItem("hetal");
        arrayList.add(pojo1);

        Pojo pojo2 = new Pojo();
        pojo2.setItem("rajan");
        arrayList.add(pojo2);

        Pojo pojo3 = new Pojo();
        pojo3.setItem("Priya");
        arrayList.add(pojo3);

        Pojo pojo4 = new Pojo();
        pojo4.setItem("Akshay");
        arrayList.add(pojo4);

        Pojo pojo5 = new Pojo();
        pojo5.setItem("Shruti");
        arrayList.add(pojo5);

        Pojo pojo6 = new Pojo();
        pojo6.setItem("bhiari");
        arrayList.add(pojo6);

        Pojo pojo7 = new Pojo();
        pojo7.setItem("rajasthani");
        arrayList.add(pojo7);

        Pojo pojo8 = new Pojo();
        pojo8.setItem("rajasthani");
        arrayList.add(pojo8);

        Pojo pojo9 = new Pojo();
        pojo9.setItem("rajasthani");
        arrayList.add(pojo9);

        Pojo pojo11 = new Pojo();
        pojo11.setItem("rajasthani");
        arrayList.add(pojo11);

        Pojo pojo22 = new Pojo();
        pojo22.setItem("rajasthani");
        arrayList.add(pojo22);

        Pojo pojo711 = new Pojo();
        pojo711.setItem("rajasthani");
        arrayList.add(pojo711);

        Pojo pojo722 = new Pojo();
        pojo722.setItem("rajasthani");
        arrayList.add(pojo722);



    }
}